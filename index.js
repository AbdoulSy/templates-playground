const nunjucks = require('nunjucks')
const fs = require('fs');

const renderTemplate = (fileName, dataBag) => {
  fs.writeFile(`dist/${fileName}`, nunjucks.render(fileName, dataBag), (err) => {
    if (err) return console.log(err)

    console.log(`The ${fileName} was saved!`)
  })
}

nunjucks.configure('templates', { autoescape: true })
renderTemplate('Dockerfile', { port: '3001' })
